import { FC, useState } from 'react';
import s from './Interview.module.scss';
import { useNavigate } from 'react-router-dom';

export const Interview: FC = () => {
    const [responses, setResponses] = useState({
        experience: '',
        tools: '',
    });

    const handleChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        const { name, value } = e.target;
        setResponses((prevResponses) => ({
            ...prevResponses,
            [name]: value,
        }));
    };

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        console.log(responses);
        // Process the responses here
    };

    const navigate = useNavigate();

    const toUsers = () => {
        navigate('/users')
    }

    return (
        <div className={s.root}>
            <h1>React Testing Library Interview</h1>
            <form onSubmit={handleSubmit}>
                <div>
                    <label>
                        Describe your experience with React Testing Library:
                        <textarea
                            name="experience"
                            value={responses.experience}
                            onChange={handleChange}
                        />
                    </label>
                </div>
                <div>
                    <label>
                        What other tools or libraries do you use for testing React
                        applications?
                        <textarea
                            name="tools"
                            value={responses.tools}
                            onChange={handleChange}
                        />
                    </label>
                </div>
                <button type="submit" id="submit-button">Submit</button>
            </form>
            <button id='toUsers' onClick={toUsers}>To Users</button>
        </div>
    );
};
