import { render, screen } from '@testing-library/react';
import { Users } from './Users'
import '@testing-library/jest-dom';

import { users } from '@/App';

describe("User", () => {
  test("renders heading", () => {
    render(<Users users={users} />)
    expect(screen.getByRole("heading", { name: "Users" })).toBeInTheDocument();
  });

  test("renders a list of users", async () => {
    render(<Users users={users} />);
    const usersElems = await screen.findAllByRole("listitem");
    expect(usersElems).toHaveLength(users.length);
  });
});