import { FC } from 'react';

import s from './Users.module.scss';

interface Props {
  users: string[];
}

export const Users: FC<Props> = ({ users }) => {
  return (
    <div className={s.root}>
      <h1>Users</h1>
      <ul>
        {users.map((user) => <li key={user}>{user}</li>)}
      </ul>
    </div>
  );
};

