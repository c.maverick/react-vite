import { Interview } from './components/Interview'
import { Users } from './components/Users'

import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";

export const users = ['user1', 'user2', 'user3']

const router = createBrowserRouter([
  {
    path: "/",
    element: <Interview />,
    errorElement: 'error',
  },
  {
    path: "/users",
    element: <Users users={users} />,
  }
]);

function App() {
  return (
    <>
      <h1>React + Vite</h1>
      <RouterProvider router={router} />
    </>
  )
}

export default App
